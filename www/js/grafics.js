
var data_frame = [];
d3.csv('data/DFC.csv', function(data_obj) {
    if (data_obj['Countries'].indexOf('"') === -1) {
        data_obj['Countries'] = JSON.parse(data_obj['Countries'].replace(/'/g, '"'));
    }
    data_frame.push(data_obj);
});

var drets_humans = {
    'EqGen': 'igualtat de gènere',
    'HrDem': 'drets democràtics',
    'Prot': 'mesures de protecció',
    'HrCp': 'drets politico-civils',
    'HrSec': 'drets socio-econòmics'
};

Plotly.d3.json('data/tf_idf.json', function(tf_idf) {
    Plotly.d3.json('data/sankey_plotly.json', function(data_json) {

    // filter based on select
    var region_sel = $('#region_sel option:selected').text();
    var hr_sel = $('#hr_sel option:selected')[0].value;
    var data_filt = data_json[region_sel][hr_sel];

    var data = {
        type: "sankey",
        domain: {
            x: [0,1],
            y: [0,1]
        },
        orientation: "h",
        valueformat: ".0f",
        valuesuffix: "TWh",
        node: {
            pad: 15,
            thickness: 15,
            line: {
                color: "black",
                width: 0.5
            },
            label: data_filt.labels,
            color: "orange"
        },
        link: {
            source: data_filt.sources,
            target: data_filt.targets,
            value: data_filt.values,
        }
    };

    var data = [data]

    var layout = {
        title: "Països i processos de pau signats entre 1990 i 2019.<br> Dades extretes del <a href='http://www.peaceagreements.org'>Peace Agreement Database</a>",
        width: 1118,
        height: 772,
        font: {
            size: 10
        },
    };

    var config = {
        displayModeBar: false,
    }

    Plotly.plot('sankey', data, layout, config=config);

    var myPlot = $('#sankey')[0];

    myPlot.on('plotly_click', function(data_click){
        // basically a PPName
        var label = data_click.points[0].label;
        if (label !== '') {
            // var id_node = data_filt.labels.findIndex((element) => element === label);
            var id_node   = data_filt.labels.indexOf(label);
            var id_source = data_filt.sources.indexOf(id_node);
            if (id_source === -1) {
                // Target
                var sidenav = $("#sidenav-left");
                sidenav.removeAttr("hidden");

                // PP
                var pp_data = $(data_frame).filter((i,n) => n['Peace Process Name'] === label);
                var dhs_counter = {};
                Object.keys(drets_humans).forEach(key => {
                    dhs_counter[key] = 0;
                });
                var countries = new Set();
                var years = {};
                for (var i=1990; i<2020; i++) {
                    years[i] = 0;
                }
                for (pp of pp_data) {
                    pp['Countries'].forEach(ctry => countries.add(ctry));
                    years[pp['Year']] += 1;
                    Object.keys(drets_humans).forEach(key => {
                        if (pp[key] !== "0") {
                            dhs_counter[key] += 1;
                        }
                    });
                }

                sidenav.find(".sidenav-content").html(` <br><br>
                                                        <h3>${label}</h3><br>
                                                        # Acords: ${pp_data.length}<br>
                                                        # Països implicats: ${Array.from(new Set(countries)).length}<br><br>
                                                        <h6 style="text-align: center;"><u>Drets humans en els acords</u></h6>
                                                        <div id=target_pie></div>
                                                        <h6 style="text-align: center;"><u>Anys signatures</u></h6>
                                                        <div id=target_years></div><br><br>
                                                        <h6 style="text-align: center;"><u>WordCloud dels acords</u></h6>
                                                        <div id=target_wordcloud></div><br><br>`);

                var data = [{
                    values: Object.keys(dhs_counter).map(key => dhs_counter[key]),
                    labels: Object.keys(drets_humans).map(key => drets_humans[key]),
                    type: 'pie'
                }];
                var layout = {
                    margin: {l:20, t:0, b:20},
                    paper_bgcolor: "rgba(0,0,0,0)", // transparent
                };
                Plotly.newPlot('target_pie', data, layout, config=config);

                // add random data to three line traces
                var data = [{
                    x: Object.keys(years),
                    y: Object.keys(years).map(key => years[key]),
                    type: 'bar'
                }];
                var layout = {
                    yaxis: { title: { text: "# acords" }},
                    margin: {l:20, t:0, b:30},
                    paper_bgcolor: "rgba(0,0,0,0)", // transparent
                    plot_bgcolor: "rgb(190, 180, 250)",
                };
                Plotly.react('target_years', data, layout, config=config);

                var layout = d3.layout.cloud()
                    .size([350, 350])
                    .words(tf_idf[label].map(function(d, i) {
                        return {text: d, size: 50-i*4};
                    }))
                    // .padding(5)
                    .rotate(function() { return 0; })
                    .font("Impact")
                    .fontSize(function(d) { return d.size; })
                    .on("end", draw_wordcloud);
                layout.start();

                function draw_wordcloud(words) {
                    d3.select("#target_wordcloud").append("svg")
                        .attr("width", layout.size()[0])
                        .attr("height", layout.size()[1])
                        .append("g")
                        .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                        .selectAll("text")
                        .data(words)
                        .enter().append("text")
                        .style("font-size", function(d) { return d.size + "px"; })
                        .style("font-family", "Impact")
                        .attr("text-anchor", "middle")
                        .attr("transform", function(d) {
                            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                        })
                        .text(function(d) { return d.text; });
                }

            } else {
                // country name
                var label = data_click.points[0].label;
                // Source
                var sidenav = $("#sidenav-right");
                sidenav.removeAttr("hidden");

                var ctry_data = $(data_frame).filter((i,n) => n['Countries'].indexOf(label) !== -1);
                var dhs_counter = {};
                Object.keys(drets_humans).forEach(key => {
                    dhs_counter[key] = 0;
                });
                var years = {};
                for (var i=1990; i<2020; i++) {
                    years[i] = 0;
                }
                for (agr of ctry_data) {
                    years[agr['Year']] += 1;
                    Object.keys(drets_humans).forEach(key => {
                        if (agr[key] !== "0") {
                            dhs_counter[key] += 1;
                        }
                    });
                }
                
                sidenav.find(".sidenav-content").html(` <br><br>
                                                        <h3>${label}</h3><br>
                                                        # Acords: ${ctry_data.length}<br><br>
                                                        <h6 style="text-align: center;"><u>Drets humans en els acords</u></h6>
                                                        <div id=source_pie></div>
                                                        <h6 style="text-align: center;"><u>Anys signatures</u></h6>
                                                        <div id=source_years></div><br><br>
                                                        <h6 style="text-align: center;"><u>WordCloud dels acords</u></h6>
                                                        <div id=source_wordcloud></div><br><br>`);

                var data = [{
                    values: Object.keys(dhs_counter).map(key => dhs_counter[key]),
                    labels: Object.keys(drets_humans).map(key => drets_humans[key]),
                    type: 'pie'
                }];
                var layout = {
                    margin: {l:20, t:0, b:20},
                    paper_bgcolor: "rgba(0,0,0,0)", // transparent
                };
                Plotly.newPlot('source_pie', data, layout, config=config);

                // add random data to three line traces
                var data = [{
                    x: Object.keys(years),
                    y: Object.keys(years).map(key => years[key]),
                    type: 'bar'
                }];
                var layout = {
                    yaxis: { title: { text: "# acords" }},
                    margin: {l:20, t:0, b:30},
                    paper_bgcolor: "rgba(0,0,0,0)", // transparent
                    plot_bgcolor: "rgb(190, 180, 250)",
                };
                Plotly.react('source_years', data, layout, config=config);

                var layout = d3.layout.cloud()
                    .size([350, 350])
                    .words(tf_idf[label].map(function(d, i) {
                        return {text: d, size: 50-i*4};
                    }))
                    // .padding(5)
                    .rotate(function() { return 0; })
                    .font("Impact")
                    .fontSize(function(d) { return d.size; })
                    .on("end", draw_wordcloud);
                layout.start();

                function draw_wordcloud(words) {
                    d3.select("#source_wordcloud").append("svg")
                        .attr("width", layout.size()[0])
                        .attr("height", layout.size()[1])
                        .append("g")
                        .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                        .selectAll("text")
                        .data(words)
                        .enter().append("text")
                        .style("font-size", function(d) { return d.size + "px"; })
                        .style("font-family", "Impact")
                        .attr("text-anchor", "middle")
                        .attr("transform", function(d) {
                            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                        })
                        .text(function(d) { return d.text; });
                }
            }
        }
        else if (label === '' && 'flow' in data_click.points[0]) {
            // Flow
        }
    });

    $(".close-sidenav").on('click', function(e) {
        $(this).parent().attr("hidden", true);
    });

    $('#region_sel').on('change', function() {
        window.location.reload();
    });

    $('#hr_sel').on('change', function() {
        window.location.reload();
    });
});
});

$( document ).ready(function() {
});